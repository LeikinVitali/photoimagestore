


import UIKit

class CustomCollectionViewCell: UICollectionViewCell {
    
    @IBOutlet weak var imageViewCell: UIImageView!
    @IBOutlet weak var imageViewCell1: UIImageView!
    @IBOutlet weak var highlightIndicator: UIView!
    @IBOutlet weak var selectIndicator: UIImageView!
        
    override var isHighlighted: Bool {
        didSet {
            self.highlightIndicator.isHidden = !isHighlighted
        }
    }
    
    override var isSelected: Bool {
        didSet {
            
            self.highlightIndicator.isHidden = !isSelected
            self.selectIndicator.isHidden = !isSelected
            
        }
    }
    
    
    
    func configure(object: UIImage ){
        self.imageViewCell.image = object
        
    }
    
}
