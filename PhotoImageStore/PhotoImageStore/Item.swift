

import Foundation

import UIKit


class Item: Codable {
    
    var imageName: String?
    var like: Bool = false
    var textComment: String?
    
    init(imageName: String, like: Bool, textComment: String ) {
        self.imageName = imageName
        self.like = like
        self.textComment = textComment
    
    }
    
    private enum CodingKeys: String, CodingKey {
        case imageName
        case textComment
        case like
    }
    
    
    required init(from decoder: Decoder) throws {
        let container = try decoder.container(keyedBy: CodingKeys.self)
        imageName = try container.decode(String.self, forKey: .imageName)
        textComment = try container.decode(String.self, forKey: .textComment)

        like = try container.decodeIfPresent(Bool.self, forKey: .like)!

    }
    
    public func encode(to encoder: Encoder) throws {
        var container = encoder.container(keyedBy: CodingKeys.self)
        
        try container.encode(self.textComment, forKey: .textComment)
        try container.encode(self.imageName, forKey: .imageName)
        try container.encode(self.like, forKey: .like)
    }
    
    
    
    
}
