

import Foundation
import UIKit

class StorageManager {
    
    enum Keys: String {
        case textKey = "keyTex"
    }
    
    private let defaults = UserDefaults.standard

    static let shared = StorageManager()
    
    private init() {}
    

    func saveObject(object: Item) {
        var array = StorageManager.shared.loadObject()
        array.append(object)
      defaults.set(encodable: array, forKey: Keys.textKey.rawValue)
    }
    
    func deleteOdject(){
        var array = StorageManager.shared.loadObject()
        array.removeAll()
      defaults.set(encodable: array, forKey: Keys.textKey.rawValue)
    }
    
    func deleteOneOdject(countObject: Int){
        var array = StorageManager.shared.loadObject()
        array.remove(at: countObject)
      defaults.set(encodable: array, forKey: Keys.textKey.rawValue)
    }
    
    func saveOneObject(object: Item, count: Int){
        var array = StorageManager.shared.loadObject()
        array.insert(object, at: count)
      defaults.set(encodable: array, forKey: Keys.textKey.rawValue)
    }
    
    func loadObject() -> [Item] {
        let array = defaults.value([Item].self, forKey: Keys.textKey.rawValue)
        
        if let array = array {
            return array
        } else  {
            return []
        }
    }


func saveImage(image: UIImage) -> String? {
        guard let documentsDirectory = FileManager.default.urls(for: .documentDirectory, in: .userDomainMask).first else { return nil}
        
        let fileName = UUID().uuidString 
        let fileURL = documentsDirectory.appendingPathComponent(fileName)
        guard let data = image.jpegData(compressionQuality: 1) else { return nil}
        
        if FileManager.default.fileExists(atPath: fileURL.path) {
            do {
                try FileManager.default.removeItem(atPath: fileURL.path)
                print("Removed old image")
            } catch let removeError {
                print("couldn't remove file at path", removeError)
            }
            
        }
        
        do {
            try data.write(to: fileURL)
            return fileName
        } catch let error {
            print("error saving file with error", error)
            return nil
        }
        
    }
    
    
    
    func loadImage(fileName:String) -> UIImage? {
        let documentDirectory = FileManager.SearchPathDirectory.documentDirectory
        
        let userDomainMask = FileManager.SearchPathDomainMask.userDomainMask
        let paths = NSSearchPathForDirectoriesInDomains(documentDirectory, userDomainMask, true)
        
        if let dirPath = paths.first {
            let imageUrl = URL(fileURLWithPath: dirPath).appendingPathComponent(fileName)
            let image = UIImage(contentsOfFile: imageUrl.path)
            return image
            
        }
        return nil
    }
    
  
    

}




extension UserDefaults {
    
    func set<T: Encodable>(encodable: T, forKey key: String) {
        if let data = try? JSONEncoder().encode(encodable) {
            set(data, forKey: key)
        }
    }
    
    func value<T: Decodable>(_ type: T.Type, forKey key: String) -> T? {
        if let data = object(forKey: key) as? Data,
            let value = try? JSONDecoder().decode(type, from: data) {
            return value
        }
        return nil
    }
}

