

import UIKit

class ImageViewController: UIViewController {
    
    // MARK: - IBOutlet
    @IBOutlet weak var myTextView: UITextView!
    @IBOutlet weak var imageView: UIImageView!
    
    // MARK: - var

    var keyboardDismissTapGesture: UIGestureRecognizer?
    var imageName: [Item] = []
    var count = 0
    
    lazy var likeBarButton: UIBarButtonItem = {
        let barButtonItem = UIBarButtonItem(image: UIImage(named: "cHeart"), style: .plain, target: self, action: #selector(didSelectButtonClicked(_:)))
        return barButtonItem
    }()
    
    
    lazy var deleteBarButton: UIBarButtonItem = {
        let barButtonItem = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(callAlertDelete(_:)))
        return barButtonItem
    }()
    
    lazy var commentBarButton: UIBarButtonItem = {
        let barButtonItem = UIBarButtonItem(barButtonSystemItem: .edit, target: self, action: #selector(commentButtonClicked(_:)))
        return barButtonItem
    }()
   
    // MARK: - viewDidLoad

    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setupImageView()
        self.swipeImage()
        self.setupBarButtonItems()
    }
    
    
    // MARK: - func

    
    private func setupBarButtonItems() {
        navigationItem.rightBarButtonItems = [self.likeBarButton, self.commentBarButton,self.deleteBarButton]
        
    }
    
    
    func jumpViewController(){
        self.navigationController?.popToRootViewController(animated: true)
    }
    
    
    
    func reloadSaveObject(){
        
        StorageManager.shared.deleteOneOdject(countObject: count)
        StorageManager.shared.saveOneObject(object: imageName[count], count: count)
    }
    
    
    func callSaveImageArray(){
        
        for item in imageName{
            StorageManager.shared.saveObject(object: item)
        }
    }
    
    
    func checkLike(){
        
        if self.imageName[count].like ==  false{
            
            self.likeBarButton.image = UIImage(named:"cHeart" )
            
        } else if self.imageName[count].like == true{
            self.likeBarButton.image = UIImage(named:"bHeart" )
            
        }
        
    }
    
    
    private func setupImageView() {
        
        self.imageView.image = StorageManager.shared.loadImage(fileName: imageName[count].imageName ?? "1")
        self.checkLike()
        self.showComment()
    }
    
    func swipeImage(){
        
        let swipeLeft = UISwipeGestureRecognizer(target: self, action: #selector(backImage))
        swipeLeft.direction = .left
        self.view.addGestureRecognizer(swipeLeft)
        
        let swipeRight = UISwipeGestureRecognizer(target: self, action: #selector(nextImage))
        swipeRight.direction = .right
        self.view.addGestureRecognizer(swipeRight)
        
    }
    
    
    
    func showComment(){
        self.myTextView.text = self.imageName[self.count].textComment
    }
    
    
    @objc func backImage(){
        count -= 1
        if count < 0{
            count = imageName.count - 1
        }
        
        self.checkLike()
        self.showComment()
        
        let imageAbroad = UIImageView(image: StorageManager.shared.loadImage(fileName: imageName[count].imageName ?? "1"))
        self.imageView.frame = CGRect(x: 0, y: Int(self.imageView.frame.origin.y), width: 0, height: 0)
        self.imageView.image = imageAbroad.image
        imageAbroad.removeFromSuperview()
        imageAbroad.image = nil
        
        
    }
    
    
    @objc func nextImage(){
        
        count += 1
        if count > (imageName.count - 1){
            count = 0
        }
        
        self.showComment()
        self.checkLike()
        
        let imageAbroad = UIImageView(image: StorageManager.shared.loadImage(fileName: imageName[count].imageName ?? "1"))
        self.imageView.frame = CGRect(x: 0, y: Int(self.imageView.frame.origin.y), width: 0, height: 0)
        self.imageView.image = imageAbroad.image
        imageAbroad.removeFromSuperview()
        
        imageAbroad.image = nil
        
    }
    
    // MARK: - IBAction

    
    @IBAction func backButton(_ sender: UIButton) {
        self.backImage()
    }
    
    
    @IBAction func nextButton(_ sender: UIButton) {
        self.nextImage()
    }
    
    
    
    // MARK: - UIBarButton

    
    @objc func commentButtonClicked(_ sender: UIBarButtonItem){
        
        let alertController = UIAlertController(title: "", message: "Добавить комментарий", preferredStyle: .alert)
        let actionOk = UIAlertAction(title: "OK", style: .default) { [self] (_) in
            
            let name = alertController.textFields?.first?.text
            self.imageName[count].textComment = name
            self.myTextView.text = name
            self.reloadSaveObject()
            
        }
        
        let actionCancel = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        
        alertController.addTextField { (name) in
            name.placeholder = "Комментарий"
        }
        
        alertController.addAction(actionCancel)
        alertController.addAction(actionOk)
        
        self.present(alertController, animated: true, completion: nil)
        
    }
    
    
    
    @objc func didSelectButtonClicked(_ sender: UIBarButtonItem) {
        
        
        if self.imageName[count].like ==  false{
            self.imageName[count].like = true
            self.likeBarButton.image = UIImage(named:"bHeart" )
            self.reloadSaveObject()
            
        } else if self.imageName[count].like == true{
            self.imageName[count].like = false
            self.likeBarButton.image = UIImage(named:"cHeart" )
            self.reloadSaveObject()
            
        }
        
    }
    
    
    @objc func callAlertDelete(_ sender: UIBarButtonItem){
        showAlertExten(title: "", message: "Вы действительно хотите удалить выбранное изображение", button: "OK", handler: { (UIAlertAction) in
            if self.self.imageName.count > 1{
                self.imageName.remove(at: self.count)
                StorageManager.shared.deleteOneOdject(countObject: self.count)
                
                self.nextImage()
            } else if self.imageName.count <= 1{
                
                self.imageName.remove(at: self.count)
                StorageManager.shared.deleteOneOdject(countObject: self.count)
                self.imageView.image = nil
                self.jumpViewController()
            }
        }, style: .default, buttonTwo: "Cancel", styleTwo: .cancel, handlerTwo: nil)
        
    }
    
}


extension UIViewController{
    
    
    func showAlertExten(title: String, message: String, button: String, handler: ((UIAlertAction) -> Void)?, style: UIAlertAction.Style, buttonTwo: String, styleTwo: UIAlertAction.Style, handlerTwo: ((UIAlertAction) -> Void)?){
        
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        
        let alertAction = UIAlertAction(title: button, style: style, handler: handler)
        alert.addAction(alertAction)
        
        let alertActionTwo = UIAlertAction(title: buttonTwo, style: styleTwo, handler: handlerTwo)
        alert.addAction(alertActionTwo)
        
        self.present(alert, animated: true, completion: nil)
        
    }
    
    
    func showAlertExten(title: String, message: String, button: String, handler: ((UIAlertAction) -> Void)?, style: UIAlertAction.Style){
        
        
        let alert = UIAlertController(title: title, message: message, preferredStyle: .alert)
        let alertAction = UIAlertAction(title: button, style: style, handler: handler)
        alert.addAction(alertAction)
        self.present(alert, animated: true, completion: nil)
    }
    
    
    
    
    
}
