import UIKit
import Foundation

class ViewController: UIViewController {
    
    enum Mode {
        case view
        case select
    }
    // MARK: - IBOutlet
    @IBOutlet weak var myCollectionView: UICollectionView!
    
    // MARK: - let
    let imagePicker = UIImagePickerController()
    let numberItemCell = 3
    let lineSpacing: CGFloat = 1
    
    // MARK: - var

    var collectionViewLayout: UICollectionViewFlowLayout!
    var selectViewIndex: [IndexPath: Bool] = [:]
    var items: [Item] = []
    var arrayImage:[UIImage] = []
    
    var mMode: Mode = .view{
        didSet{
            switch mMode {
            case .view:
                selectBarButton.title = "Select"
                navigationItem.leftBarButtonItem = nil
                self.myCollectionView.allowsMultipleSelection = false
                
            case .select:
                selectBarButton.title = "Cancel"
                navigationItem.leftBarButtonItem = deleteBarButton
                self.myCollectionView.allowsMultipleSelection = true
                
                
            }
        }
    }
    
    
    lazy var selectBarButton: UIBarButtonItem = {
        let barButtonItem = UIBarButtonItem(title: "Select", style: .plain, target: self, action: #selector(didSelectButtonClicked(_:)))
        return barButtonItem
    }()
    
    lazy var deleteBarButton: UIBarButtonItem = {
        let barButtonItem = UIBarButtonItem(barButtonSystemItem: .trash, target: self, action: #selector(didDeleteButtonClicked(_:)))
        return barButtonItem
    }()
    
    lazy var addImageButton: UIBarButtonItem = {
        let addImageButton = UIBarButtonItem(barButtonSystemItem: .add, target: self, action: #selector(addImageButton(_:)))
        return addImageButton
    }()
    
    
    // MARK: - viewDidLoad


    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.imagePicker.delegate = self
        self.setupCollectionViewCell()
        self.setupBarButtonItems()
        self.setupCollectionViewItemSize()
    }
    
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        self.items = StorageManager.shared.loadObject()
        self.myCollectionView.reloadData()
    }
    
    // MARK: - func

    
    func unpackingImage(){
        
        for item in self.items  {
            arrayImage.append((StorageManager.shared.loadImage(fileName: item.imageName ?? "1") ?? UIImage(named: "1"))!)
        }
        
    }
    
    
    
    private func setupBarButtonItems() {
        self.navigationItem.rightBarButtonItems = [addImageButton,selectBarButton]
        
    }
    
    
    func setupCollectionViewCell(){
        let nib = UINib(nibName: "CustomCollectionViewCell", bundle: nil)
        self.myCollectionView.register(nib, forCellWithReuseIdentifier: "CustomCollectionViewCell")
    }
    
    
    private func setupCollectionViewItemSize() {
        if self.collectionViewLayout == nil{
            self.collectionViewLayout = UICollectionViewFlowLayout()
           self.collectionViewLayout.scrollDirection = .vertical
            self.collectionViewLayout.minimumLineSpacing = self.lineSpacing
            self.collectionViewLayout.minimumInteritemSpacing = self.lineSpacing
            self.myCollectionView.setCollectionViewLayout(collectionViewLayout, animated: true)
        }
        
        
    }
    
    // MARK: - UIBarButton

    @objc func addImageButton(_ sender: UIBarButtonItem) {
        self.imagePicker.sourceType = .photoLibrary
        present(self.imagePicker, animated: true, completion: nil)
    }
    
    @objc func didSelectButtonClicked(_ sender: UIBarButtonItem) {
        self.mMode = mMode == .view ? .select : .view
    }
    
    @objc func didDeleteButtonClicked(_ sender: UIBarButtonItem) {
        var deleteNeededIndexPaths: [IndexPath] = []
        for (key, value) in self.selectViewIndex {
            if value {
                deleteNeededIndexPaths.append(key)
            }
        }
        
        for i in deleteNeededIndexPaths.sorted(by: { $0.item > $1.item }) {
            items.remove(at: i.item)
        }
        
        self.myCollectionView.deleteItems(at: deleteNeededIndexPaths)
        self.selectViewIndex.removeAll()
        
        StorageManager.shared.deleteOdject()
        
        for item in items {
            StorageManager.shared.saveObject(object: item)
        }
        
        
        
    }
    
}



extension ViewController: UICollectionViewDelegate, UICollectionViewDataSource, UICollectionViewDelegateFlowLayout{
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return items.count
    }
    
    
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "CustomCollectionViewCell", for: indexPath) as? CustomCollectionViewCell else{
            return UICollectionViewCell()
        }
        
        
        cell.configure(object: StorageManager.shared.loadImage(fileName: items[indexPath.item].imageName ?? "1")!)
        cell.awakeFromNib()
        
        return cell
    }
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        
        switch mMode {
        case .view:
            
            self.myCollectionView.allowsSelection = false
            
            guard let controller = self.storyboard?.instantiateViewController(withIdentifier: "ImageViewController") as? ImageViewController else{return}
            
            controller.imageName  = self.items
            controller.count = indexPath.item
            
            self.navigationController?.pushViewController(controller, animated: true)
            
            self.myCollectionView.allowsSelection = true
            
        case .select:
            self.selectViewIndex[indexPath] = true
        }
        
    }
    
    
    
    
    func collectionView(_ collectionView: UICollectionView, didDeselectItemAt indexPath: IndexPath) {
        
        if mMode == .select {
            self.selectViewIndex[indexPath] = false
        }
        
    }
    
    
    func collectionView(_ collectionView: UICollectionView, layout collectionViewLayout: UICollectionViewLayout, sizeForItemAt indexPath: IndexPath) -> CGSize{
        
        let side = self.myCollectionView.frame.size.width / CGFloat(self.numberItemCell) - self.lineSpacing
        return  CGSize(width: side, height: side)
    }
    
}






extension ViewController: UIImagePickerControllerDelegate, UINavigationControllerDelegate {
    
    @objc func imagePickerController(_ picker: UIImagePickerController, didFinishPickingMediaWithInfo info: [UIImagePickerController.InfoKey : Any]) {
        
        if let pickedImage = info[.originalImage] as? UIImage {
            
            let imageSave =  StorageManager.shared.saveImage(image: pickedImage)
            let oneImage = Item(imageName: imageSave!, like: false, textComment: "")
            
            StorageManager.shared.saveObject(object: oneImage)
            
            self.items.append(oneImage)
            picker.dismiss(animated: true, completion: nil)
            self.myCollectionView.reloadData()
            
            
        }
        
    }
    
}
